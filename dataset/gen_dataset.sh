#!/bin/bash

# check if we are inside a proper git repository
if git rev-parse --git-dir > /dev/null 2>&1; then :
	PROJECT_ROOT=$(git rev-parse --show-toplevel)
else :
	echo "Error: you must execute this script in the kmeans repository"
	exit -1
fi

echo "Generating the dataset... (it may take a while)"

PREVIOUS_CWD=$PWD
cd $PROJECT_ROOT/dataset
python $PROJECT_ROOT/dataset/generator/generate_dataset.py
cd $PREVIOUS_CWD


