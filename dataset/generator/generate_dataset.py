import random

number_of_files = 100
number_of_features = 20
numbers_of_rows = [ 1000, 7500, 4000, 12500 ]
min_value = 1
max_value = 100




def generate_homo( ):
    number_of_rows = numbers_of_rows[0]

    for file_index in range(number_of_files):
        with open('homogeneous_{0}.data'.format(file_index + 1), 'w') as outfile:
            for row_index in range(number_of_rows):
                values = ' '.join([ str(random.randint(min_value,max_value)) for x in range(number_of_features) ])
                outfile.write('{0} {1}\n'.format(row_index, values))



def generate_hetero_sorted( ):
    number_of_change_points = 3
    changes_counter = 0
    number_of_rows = numbers_of_rows[changes_counter]
    change_points = [ (number_of_files/(number_of_change_points+1))*(i+1) for i in range(number_of_change_points) ]

    for file_index in range(number_of_files):
        if file_index in change_points:
            changes_counter += 1
            number_of_rows =  numbers_of_rows[changes_counter]
        with open('heterogeneous_order_{0}.data'.format(file_index + 1), 'w') as outfile:
            for row_index in range(number_of_rows):
                values = ' '.join([ str(random.randint(min_value,max_value)) for x in range(number_of_features) ])
                outfile.write('{0} {1}\n'.format(row_index, values))


def generate_hetero( ):

    for file_index in range(number_of_files):
        number_of_rows = random.choice(numbers_of_rows)
        with open('heterogeneous_{0}.data'.format(file_index + 1), 'w') as outfile:
            for row_index in range(number_of_rows):
                values = ' '.join([ str(random.randint(min_value,max_value)) for x in range(number_of_features) ])
                outfile.write('{0} {1}\n'.format(row_index, values))



def generate_dse_data( ):
    counter = 1
    for file_index in range(number_of_files / len(numbers_of_rows)):
        for number_of_rows in numbers_of_rows:
            with open('dse_{0}.data'.format(counter), 'w') as outfile:
                for row_index in range(number_of_rows):
                    values = ' '.join([ str(random.randint(min_value,max_value)) for x in range(number_of_features) ])
                    outfile.write('{0} {1}\n'.format(row_index, values))
            counter += 1



if __name__ == '__main__':


    print('Generating the homogeneous dataset')
    generate_homo()
    print('Generating the sorted heterogeneous dataset')
    generate_hetero_sorted()
    print('Generating the heterogeneous dataset')
    generate_hetero()
    print('Generating the DSE dataset')
    generate_dse_data()
    print('All Done!')
