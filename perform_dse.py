import inspect
import os
import subprocess
import logging
import csv
import statistics

iteration_name = 'iterations'
iteration_field = 'Knob_ITERATIONS'
iteration_domain = [ 1, 2, 5, 10, 20, 50 ]

metrics = {
    'time_us':'TIME_MONITOR_average',
    'error':'ERROR_MONITOR_average'
}

feature_name = 'size'
feature_field = 'Input_SIZE'



if __name__ == '__main__':

    # provides a better interface to print information
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(__name__)

    # get and compute the relevant path
    path_project_root = os.path.realpath(os.path.dirname(inspect.getfile(inspect.currentframe())))
    path_workspace = os.path.join(path_project_root, 'build')
    path_executable = os.path.join(path_workspace, 'kmeans.x')
    path_configuration = os.path.join(path_workspace, 'clustering.log')
    path_op_homo = os.path.join(path_project_root, 'step2', 'config', 'op.xml')
    path_op_hetero = os.path.join(path_project_root, 'step3', 'config', 'op.xml')


    # check if everything exists before performing the des
    if not os.path.isfile(path_executable):
        message = 'Error: unable to locate the executable "{0}"'.format(path_executable)
        logger.error(message)
        raise ValueError(message)
    if not os.path.isdir(path_workspace):
        message = 'Error: unable to locate the workspace "{0}"'.format(path_workspace)
        logger.error(message)
        raise ValueError(message)

    # this list will contain all the line of the OPs
    content = []

    # this flag tells if we the application is using input features or not
    with_features = False

    # write the preamble
    content.append('<?xml version="1.0" encoding="UTF-8"?>')
    content.append('<points version="2.0" block="clustering">')


    # start the real Design Space Exploration
    logger.info('Starting the Design Space Exploration')
    for value_index, knob_value in enumerate(iteration_domain):

        logger.info('  Exploring point {0}/{1}'.format(value_index+1, len(iteration_domain)))

        # launch the application
        proc_handler = subprocess.Popen([path_executable, '--num_iterations', str(knob_value)], shell = False, cwd = path_workspace, stdout = subprocess.PIPE, stderr = subprocess.PIPE, stdin = subprocess.DEVNULL )
        stdoutdata, stderrdata = proc_handler.communicate()

        # make sure to have the log file (the execution went fine)
        if not os.path.isfile(path_configuration):
            message = 'Error: unable to locate the log file "{0}"'.format(path_configuration)
            logger.error(message)
            logger.error('The following is the stdout: \n {0}'.format(stdoutdata.decode('utf-8')))
            logger.error('The following is the stderr: \n {0}'.format(stderrdata.decode('utf-8')))


        # look for the metric and features
        metric_values = {}
        feature_values = []

        # open the log file
        with open(path_configuration, 'r') as infile:

            # read the csv and parse the first row for the header
            logreader = csv.DictReader(infile, delimiter = ' ')

            # go through it
            for csv_row in logreader:

                # parse the input feature
                try:
                    input_feature_value = int(csv_row[feature_field])
                    with_features = True
                except KeyError as err:
                    input_feature_value = -1
                if input_feature_value not in feature_values:
                    feature_values.append(input_feature_value)

                # parse the metrics
                for metric_name in metrics:

                    # get the value
                    metric_value = float(csv_row[metrics[metric_name]])

                    # get the inner group for cluster
                    try:
                        metric_inner_dict = metric_values[metric_name]
                    except KeyError as err:
                        metric_values[metric_name] = {}
                        metric_inner_dict = metric_values[metric_name]

                    # create the cluster for the configuration
                    try:
                        metric_inner_dict[input_feature_value].append(metric_value)
                    except KeyError as err:
                        metric_inner_dict[input_feature_value] = [ metric_value ]



        # loop over the observed features:
        for feature_value in feature_values:

            # write the Operating Point preamble
            content.append('\t<point>')
            content.append('\t\t<parameters>')
            content.append('\t\t\t<parameter name="{0}" value="{1}"/>'.format(iteration_name,knob_value))
            content.append('\t\t</parameters>')
            if with_features:
                content.append('\t\t<features>')
                content.append('\t\t\t<feature name="{0}" value="{1}"/>'.format(feature_name,feature_value))
                content.append('\t\t</features>')
            content.append('\t\t<system_metrics>')

            # write the observed metrics
            for metric_name in metric_values:
                observations = metric_values[metric_name][feature_value]
                content.append('\t\t\t<system_metric name="{0}" value="{1}" standard_dev="{2}"/>'.format(metric_name,statistics.mean(observations),statistics.stdev(observations)))

            content.append('\t\t</system_metrics>')
            content.append('\t</point>')

    content.append('</points>')

    # finally, write the content on the appropriate file
    actual_file_name = path_op_homo if not with_features else path_op_hetero
    with open(actual_file_name, 'w') as outfile:
        outfile.write('\n'.join(content))
        outfile.write('\n')
    logger.info('All Done!')
