#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <cmath>


#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include <margot.hpp>

extern "C"
{
#include "kmeans.h"
}

// *******************************************************************
// ***                      PROBLEM CONSTANT                       ***
// *******************************************************************

constexpr int number_of_features = 20;
constexpr int number_of_clusters = 5;
constexpr int reference_number_of_iterations = 50;
int num_omp_threads = 1; // used at linking time by the kmeans_clustering


inline float compute_difference( float** evaluated_data, float** reference_data,
                                 const int row_number, const int col_number )
{
  float error = 0.0f;

  for ( int row = 0; row < row_number; ++row )
  {
    float distance_accumulator = 0.0f;

    for ( int col = 0; col < col_number; ++col )
    {
      const auto diff = evaluated_data[row][col] - reference_data[row][col];
      distance_accumulator += diff * diff;
    }

    error += sqrt(distance_accumulator);
  }

  return error / static_cast<float>(row_number);
}



// *******************************************************************
// ***                      INPUT PARAMETERS                       ***
// *******************************************************************

namespace po = boost::program_options;
po::options_description opts_desc("A k-means application derived from the Rodinia Benchmark");
po::variables_map opts_vm;


void ParseCommandLine(int argc, char* argv[])
{
  // Parse command line params
  try
  {
    po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
  }
  catch (...)
  {
    std::cout << "Usage: " << argv[0] << " [options]\n";
    std::cout << opts_desc << std::endl;
    ::exit(EXIT_FAILURE);
  }

  po::notify(opts_vm);

  // Check for help request
  if (opts_vm.count("help"))
  {
    std::cout << "Usage: " << argv[0] << " [options]\n";
    std::cout << opts_desc << std::endl;
    ::exit(EXIT_SUCCESS);
  }
}


std::string dataset_root = "../dataset";
int number_of_inputs = 100;
int number_of_iterations = reference_number_of_iterations;



// *******************************************************************
// ***                     DATASET DEFINITION                      ***
// *******************************************************************

// for the single input case study
const std::string homogeneous_input = "homogeneous_";
const std::string heterogeneous_input_ordered = "heterogeneous_order_";
const std::string heterogeneous_input = "heterogeneous_";
const std::string heterogeneous_dse_input = "dse_";





// *******************************************************************
// ***                       MAIN FUNCTION                         ***
// *******************************************************************
int main( int argc, char* argv[] )
{
  margot::init();

  // Handle the program options
  opts_desc.add_options()
  ("help,h", "print this help message")
  ("dataset,d", po::value<std::string>(&dataset_root)->
   default_value(dataset_root),
   "The duration of the swaption stream (in seconds)")
  ("num_iterations,n", po::value<int>(&number_of_iterations)->
   default_value(number_of_iterations),
   "The number of iterations for finding the centroids")
  ("number_of_inputs,i", po::value<int>(&number_of_inputs)->
   default_value(number_of_inputs),
   "The number of input file to process")
  ;
  ParseCommandLine(argc, argv);
  std::cout << "Execution of k-means on " << number_of_inputs << " files" << std::endl;


  // this variable is an alias for the dataset to choose
  const std::string dataset_name = homogeneous_input;



  // loop over the input files
  for ( int file_index = 0; file_index < number_of_inputs; ++file_index )
  {
    // parse the data and put them in a safe storag
    const std::string file_name = dataset_root + "/" + dataset_name + std::to_string(file_index + 1) + ".data";
    std::vector<float> input_data;   // data read from file
    std::ifstream data_stream;
    data_stream.open(file_name);

    if (data_stream.is_open())
    {
      std::string line_string;

      while ( std::getline(data_stream, line_string) )
      {
        input_data.reserve(input_data.size() + number_of_features);
        std::stringstream line_stream(line_string);
        float datum;
        line_stream >> datum; // ingore the id of the row

        for ( int i = 0; i < number_of_features; ++i )
        {
          line_stream >> datum;
          input_data.emplace_back(datum);
        }
      }

      data_stream.close();
    }
    else
    {
      std::cerr << "Unable to open the file \"" << file_name << '\"' << std::endl;
      return EXIT_FAILURE;
    }

    const int number_of_rows = input_data.size() / number_of_features;


    float error = 0.0f; // this variable will hold the configuration error

    // start the measure
    margot::clustering::monitor::time_monitor.start();

    // --------------  Begin block of code

    // prepare the data for the C array type
    float** data_matrix = (float**) malloc(number_of_rows * sizeof(float*));

    for ( int i = 0; i < number_of_rows; ++i )
    {
      data_matrix[i] = &input_data[i * number_of_features];
    }

    // compute the cluster for each point in the file
    float** cluster_matrix = nullptr;
    cluster(number_of_rows, number_of_features, data_matrix, number_of_clusters, number_of_iterations, &cluster_matrix );

    // do something usuful with the data
    std::cout << "  - Output [" << file_index << "] -> First cluster value = " << cluster_matrix[0][0] << std::endl;


    // --------------  End block of code


    // stop the measure
    margot::clustering::monitor::time_monitor.stop();

    // compute the quality of the elaboration
    float** reference_matrix = nullptr;
    cluster(number_of_rows, number_of_features, data_matrix, number_of_clusters, reference_number_of_iterations, &reference_matrix );
    error = compute_difference(cluster_matrix, reference_matrix, number_of_clusters, number_of_features);
    margot::clustering::monitor::error_monitor.push(error);
    free(reference_matrix[0]);
    free(reference_matrix);


    // log the result
    margot::clustering::log();


    // free the memory
    free(cluster_matrix[0]);
    free(cluster_matrix);
    free(data_matrix);
  }

  return EXIT_SUCCESS;
}
