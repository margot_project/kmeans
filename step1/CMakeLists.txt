# check the minimum version
cmake_minimum_required( VERSION 2.8.12 )

# the project name
project( kmeans )

################################
#### General configure section
################################

# force the Release build if not already set
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif(NOT CMAKE_BUILD_TYPE)

# setting common c++ flags
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x -static-libgcc -Wl,--hash-style=both,--as-needed -pthread -fopenmp" )

# setting debug flags
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -g3 -O0")

# setting release with debug info flags
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} -march=native -mtune=native -g3 -O2")

# setting release flags
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native -mtune=native -O3")



################################
#### Sources
################################

# force the Release build if not already set
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif(NOT CMAKE_BUILD_TYPE)

set( KMEANS_SRC_PATH ${CMAKE_CURRENT_SOURCE_DIR}/src)
set( KMEANS_HDR_PATH ${CMAKE_CURRENT_SOURCE_DIR}/src)
set( KMEANS_HDR_FILES
  ${KMEANS_HDR_PATH}/kmeans.h
)
set( KMEANS_SRC_FILES
  ${KMEANS_SRC_PATH}/main.cc
  ${KMEANS_SRC_PATH}/cluster.c
  ${KMEANS_SRC_PATH}/kmeans_clustering.c
)

include_directories( ${KMEANS_HDR_PATH} )


################################
### External Libraries
################################

# Find Autotuner framework
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../autotuner/install/lib/cmake")
find_package(MARGOT_HEEL)

# Boost program options
find_package(Boost 1.45.0 REQUIRED program_options)


# include the directories
include_directories(${Boost_INCLUDES} ${MARGOT_HEEL_INCLUDES})


################################
#### Compilation
################################


#----- Set binary name for the mini-app
set ( EXE_NAME "${PROJECT_NAME}.x" )

add_executable(${EXE_NAME} ${KMEANS_SRC_FILES} ${KMEANS_HDR_FILES})

target_link_libraries(${EXE_NAME} ${Boost_LIBRARIES} ${MARGOT_HEEL_LIBRARIES})


################################
#### Set custom install prefix
################################

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set (
      CMAKE_INSTALL_PREFIX "${PROJECT_SOURCE_DIR}"
      CACHE PATH "default install path"
      FORCE )
endif()

################################
#### Install
################################

install( TARGETS ${EXE_NAME} DESTINATION bin )
