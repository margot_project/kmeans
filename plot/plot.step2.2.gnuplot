reset

# define the geometry of the plot
num_plots = 2

# set the terminal
set terminal pdf enhanced font 'Verdana,40' size 14,14

# set the preamble for the multiplot
set multiplot layout num_plots,1
unset title
set linestyle 1 lc rgb '#1B9E77' dt 1 lt 1 lw 6 pt 7 ps 0.4  # --- teal
set linestyle 2 lc rgb '#D95F02' dt 1 lt 1 lw 6 pt 7 ps 0.4  # --- orange
set linestyle 3 lc rgb '#7570B3' dt 1 lt 1 lw 6 pt 7 ps 0.4  # --- lilac
set linestyle 4 lc rgb '#1e90ff' dt 1 lt 1 lw 6 pt 7 ps 0.4  # --- dark magenta
set grid back
set tics nomirror
set xtics scale 0 offset 1000000
set lmargin at screen 0.16
set key above


# compute the step to increment for each plot
top_margin = 0.9
bottom_margin = 0.09
increment = (top_margin - bottom_margin) / num_plots

# initialize the margin variables
current_top = top_margin
current_bottom = current_top - increment



# ---------------------- Here there are the plot commands

set ylabel "Execution time [ms]" offset 1
set border 2
set yrange [0:]
set tmargin at screen current_top
set bmargin at screen current_bottom
current_top = current_bottom
current_bottom = current_top - increment
plot "clustering.log" u ($6/1000) with steps ls 1 title "Observed", \
	"clustering.log" u ($5/1000) with steps ls 2 title "Goal", \
	"clustering.log" u ($4/1000) with steps ls 3 title "Expected"

set ylabel "Expected Error" offset 1
set border 3
set xtics 20 scale 1 offset 0
set xlabel "Input File" offset 0,0.5
unset key
set yrange [0:50]
set ytics 0,10,40
set tmargin at screen current_top
set bmargin at screen current_bottom
current_top = current_bottom
current_bottom = current_top - increment
plot "clustering.log" u "Known_Metric_ERROR" with steps ls 1 notitle


unset multiplot
