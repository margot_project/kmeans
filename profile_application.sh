#!/bin/bash

# perform the dse
python3 perform_dse.py


# generate the gnuplot of the configurations
python $PWD/autotuner/margot_heel/margot_heel_cli/bin/margot_cli plotOPs --x_field error --y_field time_us --c_field iterations $PWD/step2/config/op.xml > ops.gnuplot
sed 's/set style circle radius screen 0.005/set style circle radius screen 0.015/g' -i ops.gnuplot
sed 's/set ylabel \"Time_us\"/set ylabel \"Execution Time \[us\]\"/g' -i ops.gnuplot
gnuplot -c ops.gnuplot > ops.pdf
evince ops.pdf
